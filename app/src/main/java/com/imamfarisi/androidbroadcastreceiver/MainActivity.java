package com.imamfarisi.androidbroadcastreceiver;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

import java.util.Calendar;
import java.util.Date;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_layout);

        Button btnBroadcast = findViewById(R.id.btnBroadcast);
        Button btnBroadcastTime = findViewById(R.id.btnBroadcastTime);

        btnBroadcast.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                sendBroadcast(new Intent("filter broadcast"));
            }
        });

        //mengirim broadcast setelah 3 detik dari waktu sekarang menggunakan alarm manager
        btnBroadcastTime.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                AlarmManager alarmManager;
                alarmManager = (AlarmManager) getSystemService(ALARM_SERVICE);
                Intent intent = new Intent(MainActivity.this, Receiver.class);
                PendingIntent alarmIntent = PendingIntent.getBroadcast(MainActivity.this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);

                Calendar calendar = Calendar.getInstance();
                calendar.setTime(new Date());
                calendar.set(Calendar.SECOND, 3);

                alarmManager.set(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), alarmIntent);
            }
        });
    }
}
